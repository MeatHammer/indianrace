package com.example.tree;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class ShopActivity extends AppCompatActivity {

    static ImageButton shopBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);
        shopBtn = (ImageButton) findViewById(R.id.shop_button);
        if (MainActivity.isShopBtnNotActive) { shopBtn.setVisibility(View.INVISIBLE); }
        else { shopBtn.setVisibility(View.VISIBLE); }
    }

    public void shopBtn_Click(View view){
        if (MainActivity.settings[0]) MainActivity.sounds.mediaPlayer.start();
        ImageButton imageButton = (ImageButton) view;
        imageButton.startAnimation(MainActivity.animClick);
        view.setVisibility(View.INVISIBLE);
        MainActivity.isShopBtnNotActive = true;
        MainActivity.lightningCounter += 3;
        MainActivity.lightningCount.setText(str(MainActivity.lightningCounter));
    }

    public void closeBtn_Click(View view){
        if (MainActivity.settings[0]) MainActivity.sounds.mediaPlayer.start();

        finish();
    }
    private String str(int n){
        return String.valueOf(n);
    }
    private String str(float n){
        return String.valueOf(n);
    }
}
