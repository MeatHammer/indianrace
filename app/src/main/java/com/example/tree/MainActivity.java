package com.example.tree;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;


public class MainActivity extends AppCompatActivity {
    public static Animation animClick;
    Animation animWaterDrop, animSmallWaterDropOne, animSmallWaterDropTwo, animSmallWaterDropThree;
    ImageView tree, wateringCan, waterDrop, smallWaterDropOne, smallWaterDropTwo, smallWaterDropThree;
    int scrWidth, scrHeight;
    LinearLayout topBar;
    boolean isWateringCanFree = true;
    public static Sounds sounds;
    public static boolean[] settings;
    public static MediaPlayer music;
    ConstraintLayout mainConstraintLayout;
    TextView dollarsScoreViewer;
    ArrayList<ImageView> dollarsOnTree;
    int dollarsScore;
    int rainSecondsPassed;
    static int shopSecondsPassed;
    ArrayList<ImageView> rainDrops = new ArrayList<ImageView>();
    float density;
    static boolean isShopBtnNotActive;
    static int lightningCounter;
    static TextView lightningCount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        animClick = AnimationUtils.loadAnimation(this, R.anim.click_btn);
        tree = (ImageView) findViewById(R.id.tree);
        wateringCan = (ImageView) findViewById(R.id.wateringCan);

        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics ();
        display.getMetrics(outMetrics);
        density  = getResources().getDisplayMetrics().density;
        Point size = new Point();
        display.getSize(size);
        scrWidth = size.x;
        scrHeight = size.y;

        waterDrop = (ImageView) findViewById(R.id.waterDrop);
        animWaterDrop = AnimationUtils.loadAnimation(this, R.anim.water_drop);
        animWaterDrop.setDuration(500);
        smallWaterDropOne = (ImageView) findViewById(R.id.smallWaterDropOne);
        animSmallWaterDropOne = AnimationUtils.loadAnimation(this, R.anim.small_water_drop_one);
        animSmallWaterDropOne.setDuration(500);
        smallWaterDropTwo = (ImageView) findViewById(R.id.smallWaterDropTwo);
        animSmallWaterDropTwo = AnimationUtils.loadAnimation(this, R.anim.small_water_drop_two);
        animSmallWaterDropTwo.setDuration(500);
        smallWaterDropThree = (ImageView) findViewById(R.id.smallWaterDropThree);
        animSmallWaterDropThree = AnimationUtils.loadAnimation(this, R.anim.small_water_drop_three);
        animSmallWaterDropThree.setDuration(500);

        sounds = new Sounds(this);
        settings = new boolean[]{true, true, true};

        music = MediaPlayer.create(this, R.raw.jazz);
        music.start();

        mainConstraintLayout = (ConstraintLayout) findViewById(R.id.mainConstraintLayout);

        dollarsOnTree = new ArrayList<ImageView>();
        dollarsScore = 0;
        dollarsScoreViewer = (TextView) findViewById(R.id.dollars);

        topBar = (LinearLayout)findViewById(R.id.topBar);

        //
        rainSecondsPassed = 0;
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            private LinearLayout scoreBar = (LinearLayout) findViewById(R.id.scoreBar);
            private LinearLayout scoreBarContainer = (LinearLayout) findViewById(R.id.scoreBarContainer);
            @Override
            public void run()
            {
                float stepBySec = scoreBarContainer.getWidth() / 60f;
                if (rainSecondsPassed < 61) {
                    rainSecondsPassed++;
                    scoreBar.setMinimumWidth((int)(rainSecondsPassed * stepBySec));
                }
                else {
                    startRain();
                    scoreBar.setMinimumWidth((int)(rainSecondsPassed * stepBySec));
                    rainSecondsPassed = 0;
                    
                }
                handler.postDelayed(this, 1000);
            }
        };
        runnable.run();
        //

        //
        addRainDrops();

        isShopBtnNotActive = true;
        startShopTimer();
        lightningCounter = 3;
        lightningCount = (TextView) findViewById(R.id.lightningCount);
        lightningCount.setText(str(lightningCounter));



    }



    
    public void startShopTimer(){
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            boolean isTimerFree = true;
            @Override
            public void run()
            {
                if (isShopBtnNotActive) {
                    if (isTimerFree) {
                        isTimerFree = false;
                        shopSecondsPassed = 0;
                    }
                    if (shopSecondsPassed < 900) {
                        shopSecondsPassed++;
                    } else {
                        try {
                            ShopActivity.shopBtn.setVisibility(View.VISIBLE);
                        }
                        catch (Exception ex) {}
                        isTimerFree = true;
                        isShopBtnNotActive = false;
                    }
                }
                handler.postDelayed(this, 1000);
            }
        };
        runnable.run();
    }

    public void addLeaf(){
        ImageView dollar = new ImageView(this);
        //dollar.setBackground(getDrawable(R.drawable.bordered));
        dollar.setElevation(2f);

        float treeCrownCenterX = tree.getLeft() + 0.601f * tree.getWidth() - 61.5f * density;
        float treeCrownCenterY = tree.getTop() + 0.273f * tree.getHeight() - 70.5f * density;
        float treeCrownRadius = tree.getHeight() * 0.258f;
        float x = new Random().nextInt(2 * (int)treeCrownRadius) - treeCrownRadius;
        float y = new Random().nextInt(2 * (int)treeCrownRadius) - treeCrownRadius;
        while (x*x + y*y > treeCrownRadius * treeCrownRadius){ // checks (x,y) for laying inside circle
            x = new Random().nextInt(2 * (int)treeCrownRadius) - treeCrownRadius;
            y = new Random().nextInt(2 * (int)treeCrownRadius) - treeCrownRadius;
        }
        float absX = x + treeCrownCenterX;
        float absY = y + treeCrownCenterY;

        dollar.setImageResource(R.drawable.dollar);
        dollar.setScaleX(0.5f);
        dollar.setScaleY(0.5f);
        dollar.setX(absX);
        dollar.setY(absY);

        dollar.setRotation(new Random().nextInt(180));
        mainConstraintLayout.addView(dollar);
        dollarsOnTree.add(dollar);

    }


    @Override
    protected void onResume() {
        super.onResume();
        try{
            music.start();
        }catch (Exception e){

        }
    }

    public void addRainDrops(){

        int step = scrWidth / 10;
        for(int i=0; i < 10; i++) {
            ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            ImageView rainDrop = new ImageView(this);
            rainDrop.setImageResource(R.drawable.water_drop);
            params.setMargins(step * (i + 1), 0, 0, 0);
            params.topToTop = R.id.mainConstraintLayout;
            params.startToStart = R.id.mainConstraintLayout;
            rainDrop.setElevation(1f);
            rainDrop.setLayoutParams(params);
            rainDrop.setVisibility(View.INVISIBLE);
            rainDrops.add(rainDrop);
            mainConstraintLayout.addView(rainDrop);
        }
    }

    public void startRain(){
        for(ImageView rainDrop : rainDrops) {
            Animation appearRainDrop = new ScaleAnimation(
                    0.5f,
                    0.7f,
                    0.5f,
                    0.7f);
            appearRainDrop.setDuration(new Random().nextInt(1000) + 1000);
            Animation moveRainDrop = new TranslateAnimation(
                    0,
                    0,
                    0,
                    tree.getHeight() + new Random().nextInt(topBar.getHeight()) + 20f); // 20f top margin
            moveRainDrop.setDuration(new Random().nextInt(1000) + 1000);
            AnimationSet animSet = new AnimationSet(false);
            animSet.addAnimation(appearRainDrop);
            animSet.addAnimation(moveRainDrop);
            rainDrop.startAnimation(animSet);
            animSet.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    addLeaf();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }

    }
    private String str(int n){
        return String.valueOf(n);
    }
    private String str(float n){
        return String.valueOf(n);
    }
    public void wateringCanBtn_Click(View view){
        if (!isWateringCanFree) { return;}
        if (MainActivity.settings[0]) MainActivity.sounds.mediaPlayer.start();
        isWateringCanFree = false;
        view.startAnimation(animClick);

        ObjectAnimator objectAnimatorTranslateY = ObjectAnimator.ofFloat(
                wateringCan,"translationY",-tree.getHeight() + 20 * density);
        ObjectAnimator objectAnimatorScaleX = ObjectAnimator.ofFloat(
                wateringCan, "scaleX", 120f);
        ObjectAnimator objectAnimatorScaleY = ObjectAnimator.ofFloat(
                wateringCan, "scaleY", 120f);
        ObjectAnimator objectAnimatorRotate = ObjectAnimator.ofFloat(
                wateringCan, "rotation", 0f, -44f);
        objectAnimatorTranslateY.setDuration(3000);
        objectAnimatorScaleX.setDuration(3000);
        objectAnimatorScaleY.setDuration(3000);
        objectAnimatorRotate.setDuration(3000);
        objectAnimatorRotate.setStartDelay(3000);
        objectAnimatorTranslateY.start();
        objectAnimatorScaleX.start();
        objectAnimatorScaleY.start();

        objectAnimatorRotate.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationEnd(animation);
                moveWaterDrops();
            }
            @Override
            public void onAnimationEnd(Animator animation){
                super.onAnimationEnd(animation);
                addLeaf();
            }
        });
        objectAnimatorRotate.start();

        objectAnimatorTranslateY = ObjectAnimator.ofFloat(
                wateringCan,"translationY",0);
        objectAnimatorScaleX = ObjectAnimator.ofFloat(
                wateringCan, "scaleX", 1f);
        objectAnimatorScaleY = ObjectAnimator.ofFloat(
                wateringCan, "scaleY", 1f);
        objectAnimatorTranslateY.setDuration(3000);
        objectAnimatorScaleX.setDuration(3000);
        objectAnimatorScaleY.setDuration(3000);
        objectAnimatorTranslateY.setStartDelay(5000);
        objectAnimatorScaleX.setStartDelay(5000);
        objectAnimatorScaleY.setStartDelay(5000);
        objectAnimatorTranslateY.start();
        objectAnimatorScaleX.start();
        objectAnimatorScaleY.start();
        objectAnimatorScaleY.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                isWateringCanFree = true;

            }
        });

    }
    public void lightningBtn_Click(View view){
        if (lightningCounter < 1) { return; }
        if (MainActivity.settings[0]) MainActivity.sounds.mediaPlayer.start();
        view.startAnimation(animClick);
        rainSecondsPassed += 20;
        lightningCounter--;
        lightningCount.setText(str(lightningCounter));
    }
    public void basketBtn_Click(View view){
        if (MainActivity.settings[0]) MainActivity.sounds.mediaPlayer.start();
        view.startAnimation(animClick);
        for (ImageView dollar : dollarsOnTree) {
            Animation moveToScore = new TranslateAnimation(
                    0,
                    dollarsScoreViewer.getX() - dollar.getX(),
                    0,
                    dollarsScoreViewer.getY() - dollar.getY());
            Animation scale = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 200f, 50f);
            AnimationSet animSet = new AnimationSet(false);
            animSet.addAnimation(moveToScore);
            animSet.addAnimation(scale);
            animSet.setDuration(500);
            dollar.startAnimation(animSet);
            mainConstraintLayout.removeView(dollar);
            dollarsScore += 100;
        }
        dollarsScoreViewer.setText(str(dollarsScore));
        dollarsOnTree.clear();
    }
    public void settingsBtn_Click(View view){
        if (MainActivity.settings[0]) MainActivity.sounds.mediaPlayer.start();
        view.startAnimation(animClick);
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);

    }

    public void plus_Click(View view){
        if (MainActivity.settings[0]) MainActivity.sounds.mediaPlayer.start();
        view.startAnimation(animClick);
        Intent intent = new Intent(this, ShopActivity.class);
        startActivity(intent);
    }

    private void moveWaterDrops(){
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams)waterDrop.getLayoutParams();
        params.setMargins(scrWidth / 2 + (int)(60 * density), tree.getTop() + (int)(30 * density), 0, 0);
        waterDrop.setLayoutParams(params);
        waterDrop.startAnimation(animWaterDrop);

        params = (ConstraintLayout.LayoutParams)smallWaterDropOne.getLayoutParams();
        params.setMargins(scrWidth / 2 + (int)(60 * density), tree.getTop() + (int)(30 * density), 0, 0);
        smallWaterDropOne.setLayoutParams(params);
        smallWaterDropOne.startAnimation(animSmallWaterDropOne);

        params = (ConstraintLayout.LayoutParams)smallWaterDropTwo.getLayoutParams();
        params.setMargins(scrWidth / 2 + (int)(60 * density), tree.getTop() + (int)(30 * density), 0, 0);
        smallWaterDropTwo.setLayoutParams(params);
        smallWaterDropTwo.startAnimation(animSmallWaterDropTwo);

        params = (ConstraintLayout.LayoutParams)smallWaterDropThree.getLayoutParams();
        params.setMargins(scrWidth / 2 + (int)(60 * density), tree.getTop() + (int)(30 * density), 0, 0);
        smallWaterDropThree.setLayoutParams(params);
        smallWaterDropThree.startAnimation(animSmallWaterDropThree);

    }

    @Override
    protected void onStop() {
        super.onStop();
        try{
            music.pause();
        }catch (Exception e){

        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{
            music.pause();
        }catch (Exception e){

        }
    }



}
