package com.example.tree;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class SettingsActivity extends AppCompatActivity {

    ImageButton[] settingsButtons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        settingsButtons = new ImageButton[3];
        settingsButtons[0] = (ImageButton) findViewById(R.id.soundBtn);
        settingsButtons[1] = (ImageButton) findViewById(R.id.musicBtn);
        //settingsButtons[2] = (ImageButton) findViewById(R.id.notificationsBtn);
        for (int i = 0; i < 3; i++) {
            if (MainActivity.settings[i]) {
                settingsButtons[i].setImageResource(R.drawable.yes);
            } else {
                settingsButtons[i].setImageResource(R.drawable.no);
            }
        }
    }

    public void sound_Click(View view) {

        if (MainActivity.settings[0]) MainActivity.sounds.mediaPlayer.start();
        ImageButton imageButton = (ImageButton) view;
        imageButton.startAnimation(MainActivity.animClick);
        if (MainActivity.settings[0]) {
            imageButton.setImageResource(R.drawable.no);
            MainActivity.settings[0] = false;
        } else {
            imageButton.setImageResource(R.drawable.yes);
            MainActivity.settings[0] = true;
        }

    }

    public void music_Click(View view) {

        if (MainActivity.settings[0]) MainActivity.sounds.mediaPlayer.start();
        ImageButton imageButton = (ImageButton) view;
        imageButton.startAnimation(MainActivity.animClick);
        if (MainActivity.settings[1]) {
            imageButton.setImageResource(R.drawable.no);
            MainActivity.settings[1] = false;
            MainActivity.music.pause();
        } else {
            imageButton.setImageResource(R.drawable.yes);
            MainActivity.settings[1] = true;
            MainActivity.music.start();
        }

    }
/*
    public void notifications_Click(View view) {

        if (MainActivity.settings[0]) MainActivity.sounds.mediaPlayer.start();
        ImageButton imageButton = (ImageButton) view;
        imageButton.startAnimation(MainActivity.animClick);
        if (MainActivity.settings[2]) {
            imageButton.setImageResource(R.drawable.no);
            MainActivity.settings[2] = false;
        } else {
            imageButton.setImageResource(R.drawable.yes);
            MainActivity.settings[2] = true;
        }

    }
*/
    public void closeBtn_Click(View view) {
        if (MainActivity.settings[0]) MainActivity.sounds.mediaPlayer.start();
        finish();
    }
}
