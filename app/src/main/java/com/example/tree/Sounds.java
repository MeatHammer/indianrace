package com.example.tree;


import android.app.Activity;
import android.media.MediaPlayer;
import java.io.Serializable;

@SuppressWarnings("serial")
public class Sounds implements Serializable {

    public MediaPlayer mediaPlayer;
    public Sounds(Activity activity){
        mediaPlayer = MediaPlayer.create(activity, R.raw.click);
    }

}
