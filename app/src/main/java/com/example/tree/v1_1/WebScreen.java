package com.example.tree.v1_1;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.webkit.PermissionRequest;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AppsFlyerLib;
import com.example.tree.MainActivity;
import com.example.tree.R;
import com.example.tree.v1_1.Util.Storage;
import com.example.tree.v1_1.Util.WVSettings;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class WebScreen extends AppCompatActivity {

    WebView webView;
    ProgressBar progressBar;


    private int GALLERY = 1, CAMERA = 2;
    private PermissionRequest mPermissionRequest;
    ValueCallback<Uri[]> callback ;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_screen);



        progressBar = findViewById(R.id.progressBar);
        initWeb();
    }


    private void initWeb(){

        webView = findViewById(R.id.web_page);
        WVSettings.setWebSetting(webView);

        webView.setWebViewClient (new WebViewClient(){
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                Log.d("PAGE LOADING",url);

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Log.e("PAGE LOADED", "onPageFinished: "+url);
                if (Storage.getProperty(Storage.LINK2) == null || !Storage.getProperty(Storage.LINK2).equals(url)){
                    saveEndPointUrl(url);
                }
                progressBar.setVisibility(View.GONE);
            }


            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                if (url.startsWith("mailto:")) {
                    Intent i = new Intent(Intent.ACTION_SENDTO, Uri.parse(url));
                    startActivity(i);
                    return true;
                } else if (url.contains("funnysmart")){

                    Intent game = new Intent(getApplicationContext() ,MainActivity.class);
                    Storage.addProperty(Storage.LINK2,"exit");
                    startActivity(game);

                    return false;
                }else if(url.contains("payment/status-success")){
                    Map<String,Object> event = new HashMap<>();
                    event.put(AFInAppEventParameterName.DESCRIPTION,"yes");
                    AppsFlyerLib.getInstance().trackEvent(getApplicationContext(),"depos",event);

                    return false;
                }
                return false;
            }

        });

        webView.setWebChromeClient(new WebChromeClient()
        {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {

                callback = filePathCallback;
                showPictureDialog();
                return true;
            }
        });

        String url = (Storage.getProperty(Storage.LINK2) != null)
                                    ? Storage.getProperty(Storage.LINK2)
                                    : Storage.getProperty(Storage.LINK1);
        webView.loadUrl(url);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState )
    {
        super.onSaveInstanceState(outState);
        webView.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        webView.restoreState(savedInstanceState);
    }


    void saveEndPointUrl(String url){
        Log.e("END_POINT", "saveEndPoint: "+ url);
        Storage.addProperty(Storage.LINK2, url);
    }

    @Override
    public void onBackPressed() {
        if (webView != null && webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    private void showPictureDialog(){
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        String[] pictureDialogItems = {
                "Select photo from gallery"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                        }
                    }
                });
        pictureDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                callback.onReceiveValue(null);
                Log.d("CANCEL","+");
            }
        });
        pictureDialog.show();
    }


    public void choosePhotoFromGallary() {

        //if(ContextCompat.checkSelfPermission(this,Manifest.permission.))

        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {

        if(ContextCompat.checkSelfPermission(this,Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            //Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            //startActivityForResult(intent, CAMERA);


            Intent pictureIntent = new Intent(
                    MediaStore.ACTION_IMAGE_CAPTURE);
            if(pictureIntent.resolveActivity(getPackageManager()) != null){
                //Create a file to store the image
                file = null;
                try {
                    file = createImageFile();
                } catch (IOException ex) {
                    // Error occurred while creating the File

                }
                if (file != null) {
                    Uri photoURI = FileProvider.getUriForFile(this, "fuming.happy.good.online.provider", file);
                    pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                            photoURI);
                    startActivityForResult(pictureIntent,
                            CAMERA);
                }
            }




        }else{
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            callback.onReceiveValue(null);
            return;
        }

        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                Log.d("SUCCES",contentURI.toString());
                callback.onReceiveValue(new Uri[]{contentURI});
                Log.d("+","ff");
            }
        }
        else if( requestCode == CAMERA ){

            ContentResolver cr =  getContentResolver();
            String path = file.getAbsolutePath();
            String imageName = null;
            String imageDescription = null;
            String uriString = null;
            try {
                uriString = MediaStore.Images.Media.insertImage(cr, path, imageName, imageDescription);
            }catch (FileNotFoundException e){

            }


            //ContentValues values = new ContentValues();
            //values.put(MediaStore.Images.Media.DATA, imageFilePath);

            callback.onReceiveValue(new Uri[]{Uri.parse(uriString)});

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {


        for (int i = 0; i < permissions.length; i++) {
            if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                //granted

            } else {
                //not granted
                callback.onReceiveValue(null);
                Toast.makeText(getApplicationContext(),"NEED APROVE THIS PERMISSION",Toast.LENGTH_SHORT).show();
                return;
            }
        }
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);



    }

    String imageFilePath;
    File file;
    private File createImageFile() throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        imageFilePath = image.getAbsolutePath();
        return image;
    }




}
