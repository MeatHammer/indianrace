package com.example.tree.v1_1.Repository;

import com.example.tree.v1_1.Model.ResponseModel;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IServerApi {

    @GET("api/user/")
    Call<ResponseModel> getUrlFromServer(@Query("model") String model,
                                         @Query("country") String country,
                                         @Query("os") int os,
                                         @Query( "numb_req")int numb_req);


    @GET("api/user/")
    Call<ResponseModel> getUrlFromServer(@Query("id") int id,
                                         @Query("apsflyer") int appsflyer,
                                         @Query("af_prt") String af_prt,
                                         @Query("model") String model,
                                         @Query("country") String country,
                                         @Query("os") int os,
                                         @Query( "numb_req")int numb_req);


    @GET("api/user/statistic/")
    Call<ResponseBody> sendStatistics(@Query("id") int id,
                                      @Query("numb_req") int numb_req);

}
