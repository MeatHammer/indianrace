package com.example.tree.v1_1.Util;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Environment;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class WVSettings {

    private WVSettings() { }

    public static void setWebSetting(WebView web){

        WebSettings WVSettings = web.getSettings();

        web.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        WVSettings.setAllowContentAccess(true);
        WVSettings.setLoadWithOverviewMode(true);
        WVSettings.setUseWideViewPort(true);
        WVSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        WVSettings.setSaveFormData(true);
        WVSettings.setJavaScriptEnabled(true);
        WVSettings.setDomStorageEnabled(true);

        WVSettings.setSupportZoom(false);
        WVSettings.setDomStorageEnabled(true);
        WVSettings.setAppCacheEnabled(true);

        WVSettings.setDatabaseEnabled(true);
        WVSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        WVSettings.setAllowFileAccess(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            WVSettings.setMixedContentMode(0);
        }
        WVSettings.setCacheMode(WVSettings.LOAD_NO_CACHE);

        CookieManager cookieManager = CookieManager.getInstance();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cookieManager.setAcceptThirdPartyCookies(web, true);
        } else {
            cookieManager.setAcceptCookie(true);
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            WVSettings.setDatabasePath("/data/data/" + web.getContext().getPackageName() + "/databases/");
        }
    }

}
