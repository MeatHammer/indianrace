package com.example.tree.v1_1;

import android.app.Application;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.Log;

import com.android.installreferrer.api.InstallReferrerClient;
import com.android.installreferrer.api.InstallReferrerStateListener;
import com.android.installreferrer.api.ReferrerDetails;
import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;
import com.example.tree.BuildConfig;
import com.example.tree.v1_1.Util.Storage;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.example.tree.v1_1.Util.Storage.AF_SUB;


public class MainApplication extends Application {

    private static final String AF_DEV_KEY = "kEAhRJP3sY83M2FX47MkS5";
    private static final String YANDEX_KEY = "f1a7d317-98ce-4df9-a0e6-b4e978f52036";



    @Override
    public void onCreate() {
        super.onCreate();

        Storage.init(getApplicationContext());


        YandexMetricaConfig config = YandexMetricaConfig.newConfigBuilder(YANDEX_KEY).build();
        YandexMetrica.activate(getApplicationContext(), config);
        YandexMetrica.enableActivityAutoTracking(this);

        AppsFlyerConversionListener conversionListener = new AppsFlyerConversionListener() {
            @Override
            public void onConversionDataSuccess(Map<String, Object> map) {
                for (String attrName : map.keySet()) {
                    Log.d("TAG",attrName);
                    if(AF_SUB.containsKey(attrName)){
                        Log.d("DATA",attrName +" : " + map.get(attrName).toString());
                        Storage.addProperty(AF_SUB.get(attrName), map.get(attrName).toString());

                    }
                }
            }

            @Override
            public void onConversionDataFail(String s) {
                Log.d("DATA",s);

            }

            @Override
            public void onAppOpenAttribution(Map<String, String> map) {
                for (String attrName : map.keySet()) {
                    Log.d("TAG",attrName);
                    if(AF_SUB.containsKey(attrName)){
                        Log.d("DATA",attrName +" : " + map.get(attrName).toString());
                        Storage.addProperty(AF_SUB.get(attrName), map.get(attrName).toString());

                    }
                }
            }

            @Override
            public void onAttributionFailure(String s) {
                Log.d("DATA",s);

            }
        };

        InstallReferrerClient referrerClient;

        referrerClient = InstallReferrerClient.newBuilder(this).build();
        referrerClient.startConnection(new InstallReferrerStateListener() {
            @Override
            public void onInstallReferrerSetupFinished(int responseCode) {
                switch (responseCode) {
                    case InstallReferrerClient.InstallReferrerResponse.OK:
                        // Соединение установлено
                        try {
                            if (BuildConfig.DEBUG) Log.d("InstallReferrerState", "OK");
                            ReferrerDetails response = referrerClient.getInstallReferrer();
                            response.getInstallReferrer();

                            if(response.getInstallReferrer()!= null){

                                String data = response.getInstallReferrer();
                                String[] pairs = data.split("&");

                                Map<String, Object> myMap = new HashMap<>();


                                for (int i=0;i<pairs.length;i++){
                                    String pair = pairs[i];
                                    String[] keyValue = pair.split("=");
                                    myMap.put(keyValue[0], keyValue[1]);
                                    
                                }
                                conversionListener.onConversionDataSuccess(myMap);

                            }
                            else
                                Log.d("ERROR","ERROR");

                            response.getReferrerClickTimestampSeconds();
                            response.getInstallBeginTimestampSeconds();
                            referrerClient.endConnection();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                        break;
                    case InstallReferrerClient.InstallReferrerResponse.FEATURE_NOT_SUPPORTED:
                        // API not available on the current Play Store app.
                        break;
                    case InstallReferrerClient.InstallReferrerResponse.SERVICE_UNAVAILABLE:
                        // Connection couldn't be established.
                        break;
                }
            }

            @Override
            public void onInstallReferrerServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
            }
        });



    }

    private void AddToStorage(){

    }


}