package com.example.tree.v1_1.Util;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;

public class Storage {


    private static final String STORAGE_NAME = "TREE";

    public static Map<String,String> AF_SUB;


    public static final String LINK1 = "url";
    public static final String LINK2 = "endpoint";
    public static final String USER_ID = "id";
    public static final String PARTNER = "sub_id_4";
    public static final String NUMB_REQ = "num_req";

    private static SharedPreferences settings = null;
    private static SharedPreferences.Editor editor = null;
    private static Context context = null;

    public static void init(Context _context){

        AF_SUB = new HashMap<>();

        AF_SUB.put("af_sub1","sub_id_1");
        AF_SUB.put("af_sub2","sub_id_2");
        AF_SUB.put("af_sub3","sub_id_3");
        AF_SUB.put("af_sub4","sub_id_4");

        context = _context;
    }

    private static void init() {
        settings = context.getSharedPreferences(STORAGE_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.apply();
    }


    public static void addProperty(String name, String value){
        if(settings==null) init();

        editor.putString(name,value);
        editor.apply();
    }

    public static String getProperty(String name){
        if(settings==null) init();
        return settings.getString(name,null);
    }

    public static void addProperty(String name, int value){
        if(settings==null) init();

        editor.putInt(name,value);
        editor.apply();
    }

    public static int getPropertyInteger(String name){
        if(settings==null) init();
        return settings.getInt(name,0);
    }

}
