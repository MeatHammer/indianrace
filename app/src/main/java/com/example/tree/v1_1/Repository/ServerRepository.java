package com.example.tree.v1_1.Repository;

import android.util.Log;

import com.example.tree.v1_1.Model.ResponseModel;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServerRepository {

    public interface ServerCallBack{
        void response(ResponseModel model);
    }

    public static ServerRepository repository;

    private String model;
    private int os;
    private String country;
    private IServerApi serverApi;
    private int numb_req = 1;

    public static ServerRepository Builder(){
        repository = new ServerRepository();
        return repository;
    }

    private ServerRepository() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://napkens-game.xyz/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        serverApi = retrofit.create(IServerApi.class);

    }

    public ServerRepository Model(String model){
        this.model = model;
        return repository;
    }
    public ServerRepository OperationSystem(int os){
        this.os = os;
        return repository;
    }
    public ServerRepository Country(String country){
        this.country = country;
        return repository;
    }


    public void GetUrlFromServer(ServerCallBack serverCallBack){

        Call<ResponseModel> modelCall = serverApi.getUrlFromServer(model,country,os,numb_req);
        modelCall.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if(response.code() == 200){
                    ResponseModel model = response.body();
                    serverCallBack.response(model);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                serverCallBack.response(null);
            }
        });
        numb_req++;
    }
    public void GetUrlFromServer(ServerCallBack ServerCallBack, String id, String af_prt){

        Call<ResponseModel> modelCall = serverApi.getUrlFromServer(Integer.valueOf(id), 1, af_prt, model,country,os,numb_req);
        modelCall.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if(response.code() == 200){
                    ResponseModel model = response.body();
                    ServerCallBack.response(model);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                ServerCallBack.response(null);
            }
        });
        numb_req++;
    }

    public void SendStatistics(String id, int num){
        Call<ResponseBody> responseBodyCall = serverApi.sendStatistics(Integer.valueOf(id), num);
        responseBodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() == 200){
                    Log.d("STATISTICS","SUCCESS");
                }else{
                    Log.d("STATISTICS","FAILED");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("STATISTICS","FAILED");
            }
        });
    }
}
